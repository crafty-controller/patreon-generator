export default {
	token  : process.env.PATREON_TOKEN, // Patron token
	cronSchedule : process.env.CRON_STRING, // Cron Schedule
	logWebhookId : process.env.DISCORD_WEBHOOK_ID, // Discord webhook log id
	logWebhookToken : process.env.DISCORD_WEBHOOK_TOKEN, // Discord webhook log token
	logMessageId : process.env.DISCORD_MESSAGE, // Post a webhook message first from like discohook then grab id
	kofi : {
		token : process.env.KOFI_TOKEN, // Ko-fi Token
		port : process.env.KOFI_PORT, // Ko-fi Port
	},
};
