# Patreon Generator 🤖

## Why 🤔
This microservice was created to remove the requirement for updating the `credits.json` file on the **crafty-controller repository** for the upcoming release of **Crafty 4**.<br>
<br>
Prior to this a dev or leadership person would need to manually update the `credits.json` which would require users to pull an update to see any changes to staffing/patrons which operationally didn't seem feasible. What this service allows is easy central updating/retirement of staff information while providing the facility of automatically appending of patron information. Recently including the ability to recieve and cache ko-fi webhook subscription events, retaining them for 30 days (configurable)<br>
<br>
This information is then pushed to our OCI bucket, where Crafty 4 instances and the Website can curl and ingest, requiring no updates to their local code.<br>

## How it works ⚙️
The way this microservice works is, On start it will login to Patreon using the provided token and pull the latest patreons and their pledge levels, then construct an object that it will moosh onto the tail of the `staff.json` provided, outputting the result to `./credits/credits.json`. (where it can be moved or symlinked to high availability, artifacted on a GitLab CI Pipeline or if the configuration is provided push it to an OCI bucket.)
<br>

## Getting Started 🚀
There are two methods of running this service:<br>
- **Scheduled**<br>
  If a `crontab` schedule is provided in `config.js` the application will **continue to run**, automatically updating the local copy of `credits.json` until termnated.<br>
  Operating on the provided schedule, we will also activly listen for changes to the `staff.json` and if a change is detected, push an update.
- **Run-once**<br>
  If no `crontab` schedule is provided the application will **run once and die**. This is ideal if you'd rather have scheduling provided by a CI pipeline where the results can then be stored in the artifact storage of that job. (NOTE: Ko-fi will not launch in this mode, ko-fi requires Scheduled mode)

## Packages we're using🏗️:

### Patreon.js ([NPM](https://www.npmjs.com/package/patreon.js) | [Gitlab](https://gitlab.com/traxam/patreon.js) | [Docs](https://trax.am/docs/patreon.js/latest/))
patreon.js is an API wrapper that makes interacting with the `Patreon REST API` as easy as pie.

### Chokidar ([NPM](https://www.npmjs.com/package/chokidar) | [GitHub](https://github.com/paulmillr/chokidar) | [Docs](https://github.com/paulmillr/chokidar/blob/master/README.md))
Minimal and efficient cross-platform file watching library

### node-cron ([NPM](https://www.npmjs.com/package/node-cron) | [GitHub](https://github.com/node-cron/node-cron) | [Docs](https://github.com/node-cron/node-cron/blob/master/README.md))
A tiny task scheduler in pure `JavaScript` for `node.js` based on `GNU crontab`. This module allows you to schedule task in `node.js` using full `crontab` syntax.

### jsonschema ([NPM](https://www.npmjs.com/package/jsonschema) | [GitHub](https://github.com/tdegrunt/jsonschema) | [Docs](https://github.com/tdegrunt/jsonschema/blob/master/README.md))
`JSON` schema validator, which is designed to be fast and simple to use.

### oci-objectstorage ([NPM](https://www.npmjs.com/package/oci-objectstorage) | [GitHub](https://github.com/oracle/oci-typescript-sdk/tree/master/lib/objectstorage) | [Docs](https://github.com/oracle/oci-typescript-sdk/blob/master/examples/javascript/objectstorage.js))
A wrapper provided by Oracle, used for interacting with their OCI 'S3 Compliant' Bucket storage.

### discord.js ([NPM](https://www.npmjs.com/package/discord.js) | [GitHub](https://github.com/discordjs/discord.js) | [Docs](https://discordjs.guide/popular-topics/webhooks.html))
Using a component of the discord.js wrapper so if webhook information is provided the application will *POST* minimal stats/report regarding when it was last-run/updated the current number of patrons etc I could honestly make this better but for what it is, it does what it needs to do.

### express ([NPM](https://www.npmjs.com/package/express) | [GitHub](https://github.com/expressjs/express) | [Docs](https://expressjs.com/en/guide/routing.html))
Express is used for receiving webhooks from ko-fi regarding new/recurring subscription events. (Really wish there was just an endpoint for this...)
