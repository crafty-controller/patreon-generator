FROM node:16.15-alpine

# Copy node files
WORKDIR /app
COPY ./package.json /app
COPY ./package-lock.json /app

# Install dependancies
RUN npm install \
    && npm prune --production

# Copy source
COPY . /app

# Start Bot
ENTRYPOINT ["npm"]
CMD ["start"]

EXPOSE 3000
