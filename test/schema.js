export const staffDataSchema = {
	'id': '/StaffData',
	'type': 'object',
	'properties': {
		'staff': {
			'$ref': '/StaffGroup',
			'required': true,
		},
		'translations': {
			'type': 'object',
			'patternProperties': {
				'[A-Za-z0-9]+': {
					'type': 'array',
					'items': {
						'type': 'object',
						'properties': {
							'name':  { 'type': 'string' },
							'status': { 'type': 'boolean' },
						},
						'minProperties': 1,
						'required': true,
					},
					'minItems': 1,
					'required': true,
				},
			},
			'minProperties': 1,
			'required': true,
		},
	},
};

export const staffGroupSchema = {
	'id': '/StaffGroup',
	'type': 'object',
	'properties': {
		'development': {
			'type': 'array',
			'items': { '$ref': '/StaffMember' },
			'minItems': 1,
			'required': true,
		},
		'support': {
			'type': 'array',
			'items': { '$ref': '/StaffMember' },
			'minItems': 1,
			'required': true,
		},
		'retired': {
			'type': 'array',
			'items': { '$ref': '/StaffMember' },
			'minItems': 1,
			'required': true,
		},
	},
};

export const staffMemberSchema = {
	'id': '/StaffMember',
	'type': 'object',
	'properties': {
		'name':  { 'type': 'string' },
		'title': { 'type': [ 'string', 'null' ] },
		'loc':   { 'type': [ 'string', 'null' ] },
		'tags':  {
			'type': 'array',
			'items': { 'type': [ 'string', 'null', 'array' ] },
			'minItems': 1,
			'required': true,
		},
		'blurb': { 'type': 'string' },
		'pic':   { 'type': [ 'string', 'null' ] },
	},
	'required': [ 'name', 'title', 'loc', 'tags', 'blurb', 'pic' ],
};
