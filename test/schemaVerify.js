import path from 'path';
import fs from 'fs';

// Setup validator & schemas
import { Validator } from 'jsonschema';
import * as schemas from './schema.js';
const v = new Validator;
v.addSchema(schemas.staffGroupSchema, '/StaffGroup');
v.addSchema(schemas.staffMemberSchema, '/StaffMember');
v.addSchema(schemas.staffDataSchema, '/StaffData');

// Setup logger
import Logger from '../src/lib/logger.js';
const logger = new Logger('CONFIG');

export function validateStaff (data) {
	// Valadate JSON Schema
	const result = v.validate(data, schemas.staffDataSchema);
	if (result.errors.length >= 1) {
		logger.error('Build aborted| Failed to parse staff.json, Please check file is correct!');
		result.errors.map(validationError => { logger.error('Validation error:', validationError.stack); });
		return false;
	}

	logger.info('Staff Validated!');
	return true;
}

function testStaff () {
	const staffPath = path.resolve('./config/staff.json');

	// Get and validate 'staff.json'
	const staffData = JSON.parse(fs.readFileSync(staffPath));
	!validateStaff(staffData) ? process.exitCode = 1 : logger.info('Schema Verification passed! :)');

}

testStaff();
