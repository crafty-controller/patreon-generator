import os from 'oci-objectstorage';
import common from 'oci-common';
import fs from 'fs';
import path from 'path';

// Setup logger
import Logger from './lib/logger.js';
const logger = new Logger('OCI');

// Define remote object (Name of object to create inside bucket)
import { jsonName } from './index.js';

export async function uploadToOCI () {
	try {
		logger.info(`Adding new '${jsonName}' to the Bucket.`);

		// Load authentication from config and create client
		const profilePath = path.resolve('./config/identity/PROFILE');
		const provider = new common.ConfigFileAuthenticationDetailsProvider(profilePath, 'DEFAULT');

		const bucket = 'crafty-credits';                               // Name of the precreated bucket to fetch
		const fileLocation = path.resolve(`./credits/${jsonName}`); // Path of the file. i.e: "/Users/location/file"

		const client = new os.ObjectStorageClient({
			authenticationDetailsProvider: provider,
		});

		// Get namespace for bucket
		const request = {};
		const response = await client.getNamespace(request);
		const namespace = response.value;

		// Create stream to upload and create request
		const stats = fs.statSync(fileLocation);
		const nodeFsBlob = new os.NodeFSBlob(fileLocation, stats.size);
		const objectData = await nodeFsBlob.getData();

		// Prepare and send request
		const putObjectRequest = {
			namespaceName: namespace,
			bucketName: bucket,
			putObjectBody: objectData,
			contentType: 'application/json',
			objectName: jsonName,
			contentLength: stats.size,
		};

		await client.putObject(putObjectRequest);
		logger.info(`Successfully added '${jsonName}' to bucket`);

	} catch (err) {
		logger.error(`Error sending '${jsonName}' to bucket`, JSON.stringify(err, null, '\t'));
	}
}
