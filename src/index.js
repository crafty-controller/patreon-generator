import path from 'path';
import fs from 'fs';
import Patreon from 'patreon.js';
import cron from 'node-cron';
import chokidar from 'chokidar';
import { sendWebhook } from './webhook.js';
import { validateStaff } from '../test/schemaVerify.js';
import { uploadToOCI } from './ociStorage.js';
import { initKofi, kofiCache, gcAndSaveKofiCache } from './kofiReciever.js';

// Setup logger
import Logger from './lib/logger.js';
const logMain = new Logger('MAIN');
const logChok = new Logger('WATCHER');
const logPat = new Logger('PATREON');
const logCron = new Logger('CRON');

// Load the config files
import config from '../config/config.js';
const client = new Patreon.PatreonAPI(config.token);
let kofiEnabled = false;

// Resolve our paths
const staffPath = path.resolve('./config/staff.json');
const creditPath = path.resolve('./credits');
export const jsonName = 'credits-v2.json';

// Patreon levels in order
const pledgeOrder = [ 'Crafty Sustainer', 'Crafty Advocate', 'Crafty Supporter' ];

// Break down 'Patreon' Payload information into usable array
function parsePledge (obj) {
	// Input should be filtered so that obj has either vanity or firstName
	const source = 'Patreon';
	const { vanity, firstName, lastName } = obj.patron;
	const level = obj.reward.title;
	let name = vanity;

	// If there is no vanity name we will use firstName and initial of lastName
	if (!vanity) {
		name = `${firstName} ${lastName}`;
	}

	return { name, level, source };
}

// Validate our 'Patreon' entries to make sure we don't parse broken payloads
function validatePledge (obj) {
	if (!obj.patron) return false; // Check if valid object
	if (!obj.patron.vanity && !obj.patron.firstName) return false;
	if (!pledgeOrder.includes(obj.reward.title)) return false;

	// If we get here return true unless most recent payment was declined, then false.
	return !obj.isInvalid();
}

// Build subscription array with patron and ko-fi(if configured) data
// Then sort array by subscrription level
async function getSubscriptions () {
	const user = await client.getCurrentUser();
	logPat.info(`Retriving Patreon Data from API User: ${user.firstName + ' ' + user.lastName}`);
	const campaigns = await client.getCurrentUserCampaigns();
	let subscriptions = [];

	// If Ko-fi reciever is enabled grab the data
	if (kofiEnabled) {
		gcAndSaveKofiCache();
		subscriptions = Object.values(kofiCache)
			.map(({ name, level, source }) => { return { name, level, source }; });
	}

	// Get our patrons and prepare them
	const patrons = (await campaigns[0].getAllPledges())
		.filter(pledge => validatePledge(pledge))
		.map(pledge => parsePledge(pledge));

	// This assumes subscription levels on both platforms are the same
	subscriptions = [
		...subscriptions,
		...patrons,
	].sort((subA, subB) => {
		let a = pledgeOrder.indexOf(subA.level);
		let b = pledgeOrder.indexOf(subB.level);

		return a - b;
	});

	// Tidy Names (Remove white space and normalize case)
	subscriptions.map(subscription => {
		const names = subscription.name.split(' ');

		for (let i = 0; i < names.length; i++) {
			if (names[i]) { names[i] = names[i][0].toUpperCase() + names[i].substr(1); }
		}

		subscription.name = names.join(' ').trim();
	});
	return subscriptions;
}


async function buildCredits () {
	try {
		// Get and validate 'staff.json'
		const staffData = JSON.parse(fs.readFileSync(staffPath));
		if (!validateStaff(staffData)) return;

		// Get Patreons and merge data
		const subs = await getSubscriptions();
		const lastUpdate = Date.now();
		const credits = Object.assign(staffData, { patrons: subs, lastUpdate });
		const subCount = Object.keys(subs).length;
		const patronCount = subCount - Object.keys(kofiCache).length;

		// Print nice log and Push to Discord Hook(if configured)
		const { development, support, retired } = credits.staff;
		const { translations } = credits;

		const statsRecipt =
		`
		${jsonName}:\t
		----------------\t
		Dev: ${development.length}
		Support: ${support.length}
		Retired: ${retired.length}
		Translations: ${Object.keys(translations).length}
		Patreons: ${patronCount}
		Ko-fi Subs: ${Object.keys(kofiCache).length}
		`;

		logMain.info(statsRecipt);
		await sendWebhook(subs, lastUpdate, statsRecipt);

		// Write our data and Push to OCI Bucket (if configured)
		fs.writeFileSync(`${creditPath}/${jsonName}`, JSON.stringify(credits, null, '\t'));
		await uploadToOCI();

	} catch (e){
		logMain.error(e);
	}
}

async function startFileWatcher (){
	// Initialise file watcher
	const watcher = chokidar.watch(staffPath, { persistent: true, awaitWriteFinish: true });
	// Add event listeners.
	watcher
		.on('ready', () => logChok.info('Initial scan complete. Ready for changes!'))
		.on('change', async path => {
			logChok.info(`Staff file ${path} has been changed`);
			await buildCredits();
		})
		.on('unlink', path => {
			logChok.error(`Staff file at ${path} has been removed unexpectedly!`);
			process.exit(1);
		})
		.on('error', e => logChok.error(e));
}

async function startCronScheduler () {
	// Start cron schedule
	logCron.info('Starting Schedule!');
	cron.schedule(config.cronSchedule, async () => {
		logCron.info('Running scheduled update!');
		await buildCredits();
	});
}

async function initPatreonGen () {
	// Bail if config is missing Patreon Token
	if (!config.token) {
		logMain.error('Patreon Token Missing! Exiting...');
		return;
	}

	// Start ko-fi reciever if config provided [DEPENDS ON CRON MODE]
	if (config.kofi.token && config.kofi.port && cron.validate(config.cronSchedule)) {
		logMain.info('Starting Ko-Fi Reciever!');
		kofiEnabled = true;
		initKofi(config.kofi.token, config.kofi.port);
	} else {
		logMain.warn('Ko-Fi Reciever not configured! Skipping...');
	}

	// Start cron and listen for staff changes if crontab expression provided.
	if (cron.validate(config.cronSchedule)){
		logMain.info('cron config found! Starting event handlers!');
		startFileWatcher();
		startCronScheduler();
	} else {
		logMain.warn('No cron schedule provided, running once!');
	}

	// Run build job once on init
	await buildCredits();
}

initPatreonGen();
