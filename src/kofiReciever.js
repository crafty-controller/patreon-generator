
import path from 'path';
import fs from 'fs';
import express from 'express';

// Setup logger
import Logger from './lib/logger.js';
const logger = new Logger('KOFI');

// Setup cache location and our shared cache
const kofiCachePath = path.resolve('./credits/kofiCache.json');
export let kofiCache = {};

// Garbage collection period (Remove entries from cache older than this)
const gcPeriod = (1000 * 60 * 60 * 24 * 30); // 30 days

export function gcAndSaveKofiCache () {
	const currentTime = Date.now();

	// Loop through cache and delete any entries that are older than gcPeriod
	Object.values(kofiCache).forEach(element => {
		if (element.time <= currentTime - gcPeriod) {
			delete kofiCache[element.name];
		}
	});

	// Write cache to disk
	fs.writeFileSync(kofiCachePath, JSON.stringify(Object.values(kofiCache), null, '\t'));
}

export function initKofi (token, port) {
	const app = new express();

	// Kofi sends payloads as 'application/x-www-form-urlencoded'
	// A field named 'data' contains the payment infomation as JSON.
	app.use(express.urlencoded({ extended: true }));

	// Read cache from file, making sure it contains fresh 🥩
	const cacheFile = JSON.parse(fs.readFileSync(kofiCachePath));
	cacheFile.forEach(element => {
		kofiCache[element.name] = element;
	});
	gcAndSaveKofiCache();

	// Setup route
	app.post('/webhook', (req, res) => {
		const payload = JSON.parse(req.body.data);

		// Bail if we can't validate the payload token
		if (payload.verification_token !== token) {
			logger.error('Subscription event - Invalid Token, ignoring... 😡🗑️');
			logger.debug(payload.from_name);
			return;
		}

		// If payment intent is not public respect this and bail
		// if (!payload.is_public) {
		// 	logger.warn('Subscription event - Not public, discarding... 😢🗑️');
		// 	logger.debug(payload.from_name);
		// 	res.status(200).end();
		// 	return;
		// }
		// Check removed, as users prefer to private on kofi, but still want in app credits

		// Discard non-subscription payments
		if (!payload.is_subscription_payment) {
			logger.warn('Subscription event - Non-subscription payment, discarding... 😢🗑️');
			logger.debug(payload.from_name);
			res.status(200).end();
			return;
		}

		// If we got here we have a valid payload to cache
		const { from_name, tier_name, timestamp } = payload;

		kofiCache[from_name] = {
			name: from_name,
			level: tier_name,
			source: 'Ko-fi',
			time: Date.parse(timestamp),
		};

		// Garbage collect and backup cache
		gcAndSaveKofiCache();
		logger.info('Subscription event Cached 💸');

		// Are we done? let Ko-fi know :)
		// Kofi Requires a 200 response
		res.status(200).end();
	});

	app.listen(port, () => logger.info(`Kofi Reciver listening on port ${port}`));
}
