import os from 'os';
import { MessageEmbed, WebhookClient } from 'discord.js';

// Import config
import config from '../config/config.js';
const { logWebhookId, logWebhookToken, logMessageId, cronSchedule } = config;
import { jsonName } from './index.js';

// Setup logger
import Logger from './lib/logger.js';
const logger = new Logger('WEBHOOK');

const webhookClient = new WebhookClient({ id: logWebhookId, token: logWebhookToken });

export async function sendWebhook (subscribers, unixTimestamp, recipt) {
	let cronMsg;

	// Bail if config is missing ANY of webhook config
	if (!logMessageId || !logWebhookId || !logMessageId) {
		logger.error('Webhook misconfigured, please check config!');
		return;
	}

	// Check if we have a cron schedule or not
	cronMsg = !cronSchedule ? 'No Schedule, Run once.' : cronSchedule;

	// Check if webhook message exists, bail if misconfigured
	try {
		// TODO | Use this for comparing old to new list
		const messageRemote = await webhookClient.fetchMessage(logMessageId);
		logger.log(messageRemote.author.username); // Just so messageRemote isn't declared and unused.
	} catch (e) {
		logger.error('Failed to fetch message: ', e.message);
		return;
	}

	// Construct Message Embed
	const statsEmbed = new MessageEmbed()
		.setColor('BLURPLE')
		.setDescription(`__**${jsonName} Overview:**__\n`
		+ '```\n'
		+ recipt
		+ '```\n');

	const patreonEmbed = new MessageEmbed()
		.setColor('#fd8400')
		.setThumbnail('http://lh3.googleusercontent.com/lhvBoHEeJZ8y6C59Q1jL_mG8rD6sHu8BOFMUgRR2ha05_y8XVEsGGTqmpgigPxG_pQ=w300')
		.setDescription('__**Patrons Supporters:**__\n'
					+ '```\n'
					+ subscribers.filter(obj => obj.source === 'Patreon').map(obj => obj.name).join(', ')
					+ '```\n');

	const kofiEmbed = new MessageEmbed()
		.setColor('#49c2fe')
		.setThumbnail('https://uploads-ssl.webflow.com/5c14e387dab576fe667689cf/61e1116779fc0a9bd5bdbcc7_Frame%206.png')
		.setDescription('__**Ko-fi Supporters:**__\n'
					+ '```\n'
					+ subscribers.filter(obj => obj.source === 'Ko-fi').map(obj => obj.name).join(', ')
					+ '```\n'
					+ `Last Update: <t:${Math.floor(unixTimestamp/1000)}>`);

	try {
		// Send Webhook
		await webhookClient.editMessage(logMessageId, {
			content:
            'PatGen is currently deployed on `'+ os.hostname() +'`\n'
            + `This embed will update on the given schedule: \`${cronMsg} | ${process.env.TZ}\`\n`
            + '__**Staff changes to the credits page must be forwarded to Leadership for approval before being updated in the patgen project !**__',
			username: 'Patreon Generator - Deployment Log',
			avatarURL: 'https://gitlab.com/uploads/-/system/project/avatar/278964/logo-extra-whitespace.png',
			embeds: [ statsEmbed, patreonEmbed, kofiEmbed ],
		});
		logger.info('Sent log to Discord!');

	} catch (e){
		logger.error(e);
	}
}
